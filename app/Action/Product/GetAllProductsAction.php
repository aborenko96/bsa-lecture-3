<?php

declare(strict_types=1);

namespace App\Action\Product;
use App\Action\Product\GetAllProductsResponse;
use App\Repository\ProductRepositoryInterface;

class GetAllProductsAction
{
    // TODO: Implement methods

    public function execute(): GetAllProductsResponse
    {
        // TODO: Implement
        $repository = app(ProductRepositoryInterface::class);

        return new GetAllProductsResponse($repository->findAll());
    }
}
