<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;
use App\Entity\Product;

class GetCheapestProductsAction
{

    public function execute(): GetCheapestProductsResponse
    {
        $repository = app(ProductRepositoryInterface::class);

        $products = collect($repository->findAll())
                    ->sortBy(function(Product $product) {
                        return $product->getPrice();
                    })->slice(0,Product::CHEAPEST_COUNT)->toArray();

        $products = array_values($products);
        
        return new GetCheapestProductsResponse($products);
    }
}
