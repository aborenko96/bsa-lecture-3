<?php

declare(strict_types=1);

namespace App\Action\Product;
use App\Repository\ProductRepositoryInterface;
use App\Entity\Product;

class GetMostPopularProductAction
{

    public function execute(): GetMostPopularProductResponse
    {
        $repository = app(ProductRepositoryInterface::class);

        $product = collect($repository->findAll())
                    ->sortByDesc(function(Product $product) {
                        return $product->getRating();
                    })->first();

        return new GetMostPopularProductResponse($product);

    }
}
