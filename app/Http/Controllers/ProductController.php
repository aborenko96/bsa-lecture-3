<?php

namespace App\Http\Controllers;

use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use App\Repository\ProductRepositoryInterface;

class ProductController extends Controller
{
   public function index(ProductRepositoryInterface $repository)
   {
       return ProductArrayPresenter::presentCollection($repository->findAll());
   }

   public function popular(GetMostPopularProductAction $action)
   {
       return ProductArrayPresenter::present($action->execute()->getProduct());
   }

   public function cheapest(GetCheapestProductsAction $action)
   {
       return view('cheap_products', ['products' => $action->execute()->getProducts()]);
   }
}