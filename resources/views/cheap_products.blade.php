<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    List of cheap products
    <ul>
        @foreach ($products as $product)
            <li>{{ $product->getName() }}: {{ $product->getPrice() }}</li>
        @endforeach
    </ul>
</body>
</html>
