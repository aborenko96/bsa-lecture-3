<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/products')->group(function() {
    Route::get('/', 'ProductController@index');
    Route::get('/popular', 'ProductController@popular');
});
