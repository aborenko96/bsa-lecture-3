<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products/cheapest', 'ProductController@cheapest');
